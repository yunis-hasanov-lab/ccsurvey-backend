package com.yunis.ccsurveybackend.models.responses

import java.util.*


open class Response (var code:Int, var message:String, var data:Any?)