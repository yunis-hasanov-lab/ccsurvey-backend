package com.yunis.ccsurveybackend.utils

open class Constants {
    companion object {
        const  val SUCCESS_MSG = "Success";
        const val SUCCESS_CODE = 200;


        const  val NO_DATA_FOUND_MSG = "No Data Found!";
        const val NO_DATA_FOUND_CODE = 404;

        const val INVALID_ARGUMENT_MSG = "InvalidArgument";
        const val INVALID_ARGUMENT_CODE = 422;


    }
}